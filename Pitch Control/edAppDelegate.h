//
//  edAppDelegate.h
//  Pitch Control
//
//  Created by Riza Fahmi on 2/17/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface edAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
