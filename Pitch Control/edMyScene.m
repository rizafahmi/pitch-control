//
//  edMyScene.m
//  Pitch Control
//
//  Created by Riza Fahmi on 2/17/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import "edMyScene.h"

@implementation edMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.0);
        
        SKTexture *charaTexture1 = [SKTexture textureWithImageNamed:@"chara1"];
        charaTexture1.filteringMode = SKTextureFilteringNearest;
        SKTexture *charaTexture2 = [SKTexture textureWithImageNamed:@"chara2"];
        charaTexture2.filteringMode = SKTextureFilteringNearest;
        
        SKAction *animate = [SKAction repeatActionForever:[SKAction animateWithTextures:@[charaTexture1, charaTexture2] timePerFrame:0.2]];
        
        _chara = [SKSpriteNode spriteNodeWithTexture:charaTexture1];
        [_chara setScale:0.50];
        _chara.position = CGPointMake(self.frame.size.width/4, CGRectGetMidY(self.frame));
        [_chara runAction:animate];
        
        _chara.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_chara.size];
        _chara.physicsBody.dynamic = YES;
        _chara.physicsBody.allowsRotation = NO;
        
        
        [self addChild:_chara];
        
        // Create ground
        SKTexture *groundTexture = [SKTexture textureWithImageNamed:@"Ground"];
        groundTexture.filteringMode = SKTextureFilteringNearest;
        SKAction *moveGroundSprite = [SKAction moveByX:-groundTexture.size.width*2 y:0 duration:0.02 * groundTexture.size.width*2];
        SKAction *resetGroundSprite = [SKAction moveByX:groundTexture.size.width*2 y:0 duration:0];
        SKAction *moveGroundSpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveGroundSprite, resetGroundSprite]]];
        
        for (int i = 0; i < 2 + self.frame.size.width / (groundTexture.size.width * 2); i++) {
            // Create The Sprite
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:groundTexture];
            [sprite setScale:2.0];
            sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height/2);
            [sprite runAction:moveGroundSpritesForever];
            [self addChild:sprite];
        }
                                              
        
        
        
        // Create ground physics
        SKNode *dummy = [SKNode node];
        dummy.position = CGPointMake(0, groundTexture.size.height);
        dummy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, groundTexture.size.height * 2)];
        dummy.physicsBody.dynamic = NO;
        [self addChild:dummy];
        
        _skyColor = [SKColor colorWithRed:113.0/255.0 green:197.0/255.0 blue:207.0/255.0 alpha:1.0];
        [self setBackgroundColor:_skyColor];
        
        
        // Create pipes
        SKTexture *_pipeTexture1 = [SKTexture textureWithImageNamed:@"Pipe"];
        _pipeTexture1.filteringMode = SKTextureFilteringNearest;
        
        SKNode *pipePair = [SKNode node];
        pipePair.position = CGPointMake(self.frame.size.width + _pipeTexture1.size.width * 2, 0);
        pipePair.zPosition = -10;
        
        CGFloat y = arc4random() % (NSInteger)(self.frame.size.height / 3);
        
        SKSpriteNode *pipe = [SKSpriteNode spriteNodeWithTexture:_pipeTexture1];
        [pipe setScale:2];
        pipe.position = CGPointMake(0, y);
        pipe.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipe.size];
        pipe.physicsBody.dynamic = NO;
        [pipePair addChild:pipe];
        
        SKAction *movePipes = [SKAction repeatActionForever:[SKAction moveByX:-1 y:0 duration:0.02]];
        [pipePair runAction:movePipes];
        
        [self addChild:pipePair];
        
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _chara.physicsBody.velocity = CGVectorMake(0, 0);
    [_chara.physicsBody applyImpulse:CGVectorMake(0, 8)];
}

CGFloat clamp(CGFloat min, CGFloat max, CGFloat value){
    if (value > max) {
        return max;
    }else if( value < min) {
        return min;
    }else{
        return value;
    }
        
}

- (void)update:(NSTimeInterval)currentTime {
    /* Called before each frame is rendered */
    //    _bird.zRotation = clamp( -1, 0.5, _bird.physicsBody.velocity.dy * ( _bird.physicsBody.velocity.dy < 0 ? 0.003 : 0.001 ) );

    _chara.zRotation = clamp(-1, 0.5, _chara.physicsBody.velocity.dy * (_chara.physicsBody.velocity.dy < 0 ? 0.003 : 0.001));
}

@end
